---
title: "Building JBoss Service Archives (SARs) with Maven 2"
author: "Brian Matthews"
date: 2009-05-29T00:00:00+00:00
slug: "building-jboss-service-archives-sars-with-maven-2"
year: "2009"
month: "2009/05"
tags: ["jboss", "sars", "maven"]
categories: ["programming"]
draft: false
---

I discovered the following Maven 2 plugins capabable of building [JBoss Service ARchives (SARs)](http://www.jboss.org/community/wiki/ServiceArchive).

* [JBoss Maven Plugin](http://mojo.codehaus.org/jboss-maven-plugin/)
* [Maven SAR Plugin](http://maven-sar.sourceforge.net/)

The JBoss Maven Plugin is the better of the two because it will bundle the project dependencies into the generated SAR along with the compiled classes and resources. Whereas the Maven SAR plugin only bundles the compiled classes and resources.

The following POM extract demonstrates the usage of the JBoss Maven Plugin:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project
  xmlns="http://maven.apache.org/POM/4.0.0"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="
    http://maven.apache.org/POM/4.0.0
    http://maven.apache.org/maven-v4_0_0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <packaging>jboss-sar</packaging>
  <!-- ... -->
  <build>
    <plugins>
      <plugin>
        <groupId>org.codehaus.mojo</groupId>
        <artifactId>jboss-packaging-maven-plugin</artifactId>
        <version>2.0-beta-1</version>
        <extensions>true</extensions>
      </plugin>
      <!-- ... -->
    </plugins>
  </build>
  <!-- ... -->
</project>
```