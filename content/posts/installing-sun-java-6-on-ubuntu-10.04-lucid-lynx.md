---
title: "Installing Sun Java 6 on Ubuntu 10.04 (Lucid Lynx)"
slug: "installing-sun-java-6-on-ubuntu-10.04-lucid-lynx"
author: "Brian Matthews"
date: 2010-07-01T00:00:00+00:00
year: "2010"
month: "2010/07"
tags: ["java", "ubuntu"]
categories: ["programming"]
draft: false
----

I came across this [post](http://happy-coding.com/install-sun-java6-jdk-on-ubuntu-10-04-lucid/) that almost completely explains how to get [Sun Java SE 6](http://java.sun.com/javase/) installed on [Ubuntu](http://www.ubuntu.com/) 10.04 (Lucid Lynx).

```bash
sudo apt-get install python-software-properties
sudo add-apt-repository "deb http://archive.canonical.com/ lucid partner"
sudo apt-get update
sudo apt-get install sun-java6-jdk
```

The problem I had with the original post was that add-apt-repository was not available until I installed the python-software-properties package.