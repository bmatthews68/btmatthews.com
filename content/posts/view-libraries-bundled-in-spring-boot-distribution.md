---
title: "View libraries bundled in Spring Boot distribution"
date: 2017-10-30T00:00:00-00:00
author: Brian Matthews
draft: false
slug: "view-libraries-bundled-in-spring-boot-distribution"
year: "2017"
month: "2017/10"
tags: ["spring-boot"]
categories: ["programming"]
---

This the command that I use when I want to see what dependencies got bundled into my Spring Boot distribution JAR files:

```bash
jar -tvf filename.jar | grep jar$ | awk '{print $8}' | sed 's/BOOT-INF\/lib\///' | sort
```