---
title: "Deploying Archiva to JBoss 4.2"
author: "Brian Matthews"
date: 2009-02-20T00:00:00+00:00
slug: "deploying-archiva-to-jboss-4.2"
year: "2009"
month: "2009/02"
tags: ["archiva", "jboss"]
categories: ["programming"]
---

After jumping through a few hoops I managed to get [Archiva](http://archiva.apache.org/) 1.1.3 successfully deployed to [JBoss](http://jboss.org/) 4.2.3.

Set the *appserver.base* and *appserver.home* properties by adding the following lines to *$JBOSS_HOME/bin/run.sh* and restart JBoss.

```bash
JAVA_OPTS="-Dappserver.home=$JBOSS_HOME -Dappserver.base=$JBOSS_HOME $JAVA_OPTS"
```

Now create data source deployment descriptors for the *archiva* and *users* databases and copy them to *$JBOSS_HOME/server/default/deploy*. I am using MySQL 5.1 to as my database server.

*archiva-ds.xml*

```xml
<?xml version="1.0" encoding="UTF-8"?>
<datasources>
  <local-tx-datasource>
    <jndi-name>jdbc/archiva</jndi-name>
    <connection-url>jdbc:mysql://localhost/archiva/connection-url>
    <driver-class>com.mysql.jdbc.Driver</driver-class>
    <user-name>archiva</user-name>
    <password><!-- Password --></password>
  </local-tx-datasource>
</datasources>    
```

*users-ds.xml*

```xml
<?xml version="1.0" encoding="UTF-8"?>
<datasources>
  <local-tx-datasource>
    <jndi-name>jdbc/users</jndi-name>
    <connection-url>jdbc:mysql://localhost/users/connection-url>
    <driver-class>com.mysql.jdbc.Driver</driver-class>
    <user-name>archiva</user-name>
    <password><!-- Password --></password>
  </local-tx-datasource>
</datasources>
```

Add *WEB-INF/jboss-web.xml* to the *.war* file.

*WEB-INF/jboss-web.xml*

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE jboss-web
  PUBLIC "-//JBoss//DTD Web Application 2.4//EN"
  "http://www.jboss.org/j2ee/dtd/jboss-web_4_0.dtd">
<jboss-web>
  <context-root>archiva</context-root>
  <resource-ref>
    <description>Archiva Database</description>
    <res-ref-name>jdbc/archiva</res-ref-name>
    <res-type>javax.sql.DataSource</res-type>
    <jndi-name>java:/jdbc/archiva</jndi-name>
    <res-auth>Container</res-auth>
  </resource-ref>
  <resource-ref>
    <description>Users Database</description>
    <res-ref-name>jdbc/users</res-ref-name>
    <res-type>javax.sql.DataSource</res-type>
    <jndi-name>java:/jdbc/users</jndi-name>
    <res-auth>Container</res-auth>
  </resource-ref>
  <resource-ref>
    <description>Mail Session</description>
    <res-ref-name>mail/Session</res-ref-name>
    <res-type>javax.mail.Session</res-type>
    <jndi-name>java:/Mail</jndi-name>
    <res-auth>Container</res-auth>
  </resource-ref>
</jboss-web>
```

Remove the following from the *WEB-INF/lib* folder of *apache-archiva-1.1.3.war* because they will cause conflicts with:

* commons-logging-api-1.1.jar
* log4j-1.2.14.jar

Copy *apache-archiva-1.1.3.war* to *$JBOSS_HOME/server/default/deploy*.