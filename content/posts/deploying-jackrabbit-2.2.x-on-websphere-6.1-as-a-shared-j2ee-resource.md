---
title: "Deploying Jackrabbit 2.2.x on WebSphere 6.1 as a Shared J2EE Resource"
slug: "deploying-jackrabbit-2.2.x-on-websphere-6.1-as-a-shared-j2ee-resource"
author: "Brian Matthews"
date: 2011-12-22T00:00:00+00:00
year: "2011"
month: "2012/12"
tags: ["java", "jackrabbit", "jcr", "websphere", "j2ee"]
categories: ["programming"]
draft: false
---

On my current assignment we are developing a multi-tennant web application and are giving the individual tennants the ability to customize the look and feel of the application. We are using [Jackrabbit](http://jackrabbit.apache.org/) to host the style sheets, images and scripts that for each of the look and feels.

Initially I tried to deploy Jackrabbit as using the [Shared J2EE Resource](http://jackrabbit.apache.org/shared-j2ee-resource-howto.html) deployment model on WebSphere 6.1. Due to a leathal cocktail of stupidity and environmental constraints I couldn’t get it working earlier in the project and decided to settle for the [Application Bundle](http://jackrabbit.apache.org/application-bundle-howto.html) deployment model.

Now we need to have more that one instance of the application running in the test environment and we’ve had to finally figure out how to get the Shared J2EE Resource deployment model up and running.

It turned out to be a lot easier than I exepcted. In order to get Jackrabbit working I just needed to customize the ra.xml deployment descriptor from jackrabbit-jca-2.2.x.rar by changing the *<transaction-support/>* element to from *XATransaction* to *NoTransaction*.

*ra.xml*

```xml
<?xml version="1.0" encoding="UTF-8"?>
<connector
    xmlns="http://java.sun.com/xml/ns/j2ee"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="
        http://java.sun.com/xml/ns/j2ee
        http://java.sun.com/xml/ns/j2ee/connector_1_5.xsd" 
    version="1.5" >
    <display-name> Apache Jackrabbit JCR Adapter</display-name>
    <vendor-name> The Apache Software Foundation</vendor-name>
    <eis-type> JCR Adapter</eis-type>
    <resourceadapter-version> 1.0</resourceadapter-version>
    <resourceadapter>
        <resourceadapter-class>org.apache.jackrabbit.jca.JCAResourceAdapter </resourceadapter-class>
        <outbound-resourceadapter>
            <connection-definition>
                <managedconnectionfactory-class>org.apache.jackrabbit.jca.JCAManagedConnectionFactory</managedconnectionfactory-class>
                <config-property>
                    <config-property-name>RepositoryURI</config-property-name>
                    <config-property-type>java.lang.String</config-property-type>
                </config-property>
                <config-property>
                    <config-property-name>HomeDir</config-property-name>
                    <config-property-type>java.lang.String</config-property-type>
                </config-property>
                <config-property>
                    <config-property-name>ConfigFile</config-property-name>
                    <config-property-type>java.lang.String</config-property-type>
                </config-property>
                <connectionfactory-interface>javax.jcr.Repository</connectionfactory-interface>
                <connectionfactory-impl-class>org.apache.jackrabbit.jca.JCARepositoryHandle</connectionfactory-impl-class>
                <connection-interface>javax.jcr.Session</connection-interface>
                <connection-impl-class>org.apache.jackrabbit.jca.JCASessionHandle</connection-impl-class>
            </connection-definition>
            <transaction-support>NoTransaction</transaction-support>
            <reauthentication-support>false</reauthentication-support>
        </outbound-resourceadapter>
    </resourceadapter>
</connector> 
```

This change is necessary because Jackrabbit does not support local transactions and actually raises an exception when called by WebSphere.

Rather than have to customise this by hand I a put together the [Maven](http://maven.apache.org/) POM below to build custom RAR for me. You must place the ra.xml deployment descriptor in *src/main/rar/META-INF*.