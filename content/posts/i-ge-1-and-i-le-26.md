---
title: "i >= 1 && i <= 26"
slug: "i-ge-1-and-i-le-26"
author: "Brian Matthews"
date: 2010-07-02T00:00:00+00:00
year: "2010"
month: "2010/07"
tags: []
categories: ["programming", "politics"]
draft: false
---

Almost 90 years after it happened, I’ve been forced to recognise partition for the first time in my life.

And the irony is that it is the Irish government that is forcing me to do it. I’ve had to add some validation logic to a drop down list for an e-government web application I’m working on. The drop down list only contains 26 counties!