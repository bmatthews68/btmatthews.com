---
title: "Déjà Vu – Using the Maven Assembly Plugin correctly"
author: "Brian Matthews"
date: 2009-06-04T00:00:00+00:00
slug: "deja-vue-using-the-maven-assembly-plugin-correctly"
draft: false
year: "2009"
month: "2009/06"
---

If you want to create an archive (.zip, .tar.gz, etc.) file as part of a [Maven 2](http://maven.apache.org/) build you need to use the [Assembly Plug-in](http://maven.apache.org/plugins/maven-assembly-plugin/). Furthermore, if you want to install it into your local repository or deploy it to a remote repository you will need the assistance of the [Builder Helper Plug-in](http://mojo.codehaus.org/build-helper-maven-plugin/).

But if you are building a multi-module project you need to do it properly or you can severely mess up your build. I’ve just spent the last 2 weeks going round in circles trying to figure out why we couldn’t build our project from the root project. In the end it turned out to be the fact that I was incorrectly using the *assembly* goal instead of the *single* goal. This was causing Maven to fork and try to build all the project dependencies again. To us it looked like our dependencies were not getting installed into the local repository and it was going out to get the last good snapshot build of the components from our remote repository which were 2 weeks out of date by this time.

Of course. Once I found the solution a little light bulb went off and I remembered I’d already seen this problem before. About two years ago in fact.

Here is a sample POM snippet that assumes an assembly descriptor with an id of *bin* and a format of *.tar.gz*:

```xml
<build>
  <plugins>
    <plugin>
      <groupId>org.apache.maven.plugins</groupId>
      <artifactId>maven-assembly-plugin</artifactId>
      <executions>
        <execution>
          <id>package</id>
          <goals>
            <goal>single</goal>
            </goals>
          <phase>package</phase>
        </execution>
      </executions>
      <configuration>
        <!-- Configure filters & descriptors -->
      </configuration>
    </plugin>
    <plugin>
      <groupId>org.codehaus.mojo</groupId>
      <artifactId>build-helper-maven-plugin</artifactId>
      <executions>
        <execution>
          <id>attach-artifacts</id>
          <phase>package</phase>
          <goals>
            <goal>attach-artifact</goal>
          </goals>
          <configuration>
            <artifacts>
              <artifact>
                <file>${project.build.directory}/${project.artifactId}-${project.version}-bin.tar.gz</file>
                <type>tar.gz</type>
                <classifier>bin</classifier>
              </artifact>
            </artifacts>
          </configuration>
        </execution>
      </executions>
    </plugin>
  </plugins>
</build>
```