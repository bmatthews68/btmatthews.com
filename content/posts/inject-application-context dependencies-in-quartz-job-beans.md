---
title: "Inject application context dependencies in Quartz job beans"
slug: "inject-application-context dependencies-in-quartz-job-beans"
author: "Brian Matthews"
date: 2011-09-24T00:00:00+00:00
year: "2011"
month: "2011/09"
tags: ["spring", "quartz"]
categories: ["programming"]
draft: false
---

The [SpringBeanJobFactory](http://static.springsource.org/spring/docs/3.0.x/javadoc-api/org/springframework/scheduling/quartz/SpringBeanJobFactory.html) allows you to inject properties from the scheduler context, job data map and trigger data entries into the job bean. But there is no way out of the box to inject beans from the application context.

So I came up with the factory bean below that extends the SpringBeanJobFactory to add auto-wiring support:

```java
import org.quartz.spi.TriggerFiredBundle;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.scheduling.quartz.SpringBeanJobFactory;
 
public final class AutowiringSpringBeanJobFactory
    extends SpringBeanJobFactory
    implements ApplicationContextAware {
 
  private transient AutowireCapableBeanFactory beanFactory;
 
  public void setApplicationContext(
      final ApplicationContext context) {
    beanFactory = context.getAutowireCapableBeanFactory();
  }
 
  @Override
  protected Object createJobInstance(
        final TriggerFiredBundle bundle)
      throws Exception {
    final Object job = super.createJobInstance(bundle);
    beanFactory.autowireBean(job);
    return job;
  }
}
```