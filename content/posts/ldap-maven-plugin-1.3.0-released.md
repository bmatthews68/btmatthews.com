---
title: "LDAP Maven Plugin 1.3.0 Released"
date: 2016-02-14T00:00:00+00:00
author: "Brian Matthews"
draft: false
slug: "ldap-maven-plugin-1.3.0-released"
year: "2016"
month: "2016/02"
tags: ["maven","ldap"]
categories: ["programming"]
---

The [LDAP Maven Plugin](http://ldap-maven-plugin.btmatthews.com/) was relying on the [jcabi-aether](http://aether.jcabi.com/) library to resolve dependencies. When I first wrote the LDAP Maven Plugin I couldn’t find much information on how to resolve dependencies and jcabi-aether proved to be invaluable. [Aether](http://www.eclipse.org/aether/) is dependency resolution framework used under the hood by Maven 3 and higher.

Aether was originally managed by [Sonatype](http://www.sonatype.com/) but it was moved to stewardship of the [Eclipse](https://eclipse.org/) foundation. Since Maven 3.1 the Eclipse version has been used.

Since I wanted to the maintain support for Maven 3.0.5 I decided to revert to the Maven 2 dependency resolution API.

In addition, I downgraded the version of [ApacheDS](https://directory.apache.org/apacheds/) used by the plugin to 1.5.5. I was having trouble  resolving one of the transitive dependencies for 2.0.0-M15 during integration testing and I didn’t have any luck using the latest version (2.0.0-M17) or later. So I decided to downgrade for now and revisit the issue when ApacheDS 2.0.0 is released and documentation on embedding it becomes available.