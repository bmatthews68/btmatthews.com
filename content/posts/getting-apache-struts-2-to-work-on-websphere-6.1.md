---
title: "Getting Apache Struts 2 to work on WebSphere 6.1"
slug: "getting-apache-struts-2-to-work-on-websphere-6.1"
author: "Brian Matthews"
date: 2012-02-24T00:00:00+00:00
year: "2012"
month: "2012/02"
tags: ["struts2", "websphere", "java", "j2ee"]
categories: ["programming"]
draft: false
---

On a recent project I had problems deploying a web application built on the [Struts 2 MVC](http://struts.apache.org/) framework to a fresh [WebSphere](http://www.ibm.com/software/websphere/) installation. I wasted a lot of time labouring under the incorrect assumption (for once) that the architectural changes I had introduced in the latest sprint were the cause of my troubles.

But it turned out to be an environmental issue. We were deploying Struts 2 as a servlet filter and out of the box WebSphere does not support HTTP requests that are served by a servlet filter. A custom property called *com.ibm.ws.webcontainer.invokefilterscompatibilty* must be set for the web container. I guess I should have just deployed straight to the UAT environment!

Here is the procedure for setting the *com.ibm.ws.webcontainer.invokefilterscompatibility* custom property:

. In the administrative console click *Servers > Application Servers >* server_name *> Web Container Settings > Web Container*.
. Under *Additional Properties* select *Custom Properties*.
. On the Custom Properties page, click *New*.
. On the settings page, enter *com.ibm.ws.webcontainer.invokefilterscompatibility* in the *Name* field and *true* in the *Value* field.
. Click *Apply* or *OK*.
. Click *Save* on the console task bar to save your configuration changes.
. Restart the server.
