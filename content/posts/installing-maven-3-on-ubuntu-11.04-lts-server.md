---
title: "Installing Maven 3 on Ubuntu 11.04 LTS Server"
slug: "installing-maven-3-on-ubuntu-11.04-lts-server"
author: "Brian Matthews"
date: 2011-08-04T00:00:00+00:00
year: "2011"
month: "2011/08"
tag: ["maven", "ubuntu"]
categories: ["programming"]
----

The instructions below are based on [Installing Maven 3 on Ubuntu 10.04 LTS Server](http://lukieb.wordpress.com/2011/02/15/installing-maven-3-on-ubuntu-10-04-lts-server/) from [Luke Bourke](http://www.linkedin.com/in/lukebourke). I am installing [Maven](http://maven.apache.org/) 3.0.3 on [Ubuntu](http://www.ubuntu.com/) 11.04 LTS Server with Oracle Java 6.

Download the Maven 3.0.3 binary distribution from your local [mirror](
http://www.apache.org/dyn/closer.cgi/maven/binaries/apache-maven-3.0.3-bin.tar.gz):

```bash
wget http://ftp.heanet.ie/mirrors/www.apache.org/dist/maven/binaries/apache-maven-3.0.3-bin.tar.gz
```

Uncompress the binary distribution and copy it to the */usr/local directory*:

```bash
tar -zxf apache-maven-3.0.3-bin.tar.gz
sudo cp -R apache-maven-3.0.3 /usr/local
```

Create symbolic link in */usr/bin*:

```bash
sudo ln -s /usr/local/apache-maven-3.0.3/bin/mvn /usr/bin/mvn
```