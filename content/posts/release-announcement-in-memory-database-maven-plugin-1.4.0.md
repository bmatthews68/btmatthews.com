---
title: "Release Announcement – In-Memory Database Maven Plugin – 1.4.0"
author: "Brian Matthews"
date: 2014-01-08T00:00:00+00:00
slug: "release-announcement-in-memory-database-maven-plugin-1.4.0"
year: "2014"
month: "2014/01"
tags: ["maven","database","testing"]
categories: ["programming"]
draft: false
---

I am pleased to announce the availability of the [In-Memory Database Maven Plugin](http://inmemdb-maven-plugin.btmatthews.com/) version 1.4.0.

This release adds two new configuration settings:

* *<skip/>* can be used to skip executing the plugin goals.
* *<attributes/>* can be used to append additional options to the JDBC url connection string.

### Using <skip/>

The *<skip/>* configuration option can be specified in the *<configuration/>* section for the plugin or an individual plugin execution. Typically this would be used to disable unit or integration tests:

```xml
<skip>${maven.test.skip}</skip>
```

The same can also be achieved passing the *inmemdb.skip* system property on the Maven command line:

```bash
mvn -Dinmemdb.skip clean install
```

### Using <attributes/>

Additional attributes can be appended to the JDBC URL connection string using the *<attributes/>* element nested inside the *<configuration/>* section for the plugin or an individual plugin execution. For example:

```xml
<attributes>
    <territory>ga_IE</territory>
</attributes>
```

### Maven Central Coordinates

The *In-Memory Database Maven Plugin* has been published in [Maven Central](http://search.maven.org/) at the following coordinates:

```xml
<plugin>
    <groupId>com.btmatthews.maven.plugins.inmemdb</groupId>
    <artifactId>inmemdb-maven-plugin</artifactId>
    <version>1.4.0</version>
</plugin>
```

### Credits

This project contains contributions from:

* [Glen Mazza](https://github.com/gmazza)
* [Nicolas Girot](https://github.com/ngirot)

### License

The In-Memory Database Maven Plugin is made available under the [Apache License](http://www.apache.org/licenses/LICENSE-2.0.html) and the source code is hosted on [GitHub](http://github.com/) at https://github.com/bmatthews68/inmemdb-maven-plugin.
