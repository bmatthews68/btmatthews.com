---
title: "Installing Photomatix pre-sets on Mac OS X"
author: "Brian Matthews"
date: 2014-08-31T00:00:00+00:00
slug: "installing-photomatix-pre-sets-on-mac-os-x"
year: "2014"
month: "2014/08"
tags: ["hdr"]
categories: ["photography"]
draft: false
---

I downloaded a load of [Photomatix](http://www.hdrsoft.com/) pre-sets from http://alikgriffin.com/. They came as an archive of .xmp files without any instructions on how to install them.

Fortunately I had already created and saved a pre-set so I just had to locate that on the file system. I found it in my *~/Application Support/Photmatix/Presets* folder. Then I just dragged the contents of the archive into that folder and now I have 25 shiny new pre-sets to play with.