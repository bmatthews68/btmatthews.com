---
title: "Release Announcement: Selenium JUnit 4 Runner 1.0"
slug: "release-announcement-selenium-junit-4-runner-1.0"
author: "Brian Matthews"
date: 2011-09-11T00:00:00+00:00
year: "2011"
month: "2011/09"
tags: ["selenium", "junit", "java"]
categories: ["programming"]
draft: false
---

I recently released [Selenium JUnit 4 Runner](http://selenium-junit4-runner.btmatthews.com/). It is an extension for [JUnit 4](http://junit.org/) providing a test runner to execute http://seleniumhq.org/[Selenium] test cases. Both the Selenium 1.0 (Server) and 2.0 (Web Driver) APIs are supported.

Here is a very simple example:

```java
@RunWith(SeleniumJUnit4ClassRunner.class)
public class TestGoogleSearch {
    @WebDriverConfiguration
    public GoogleHomePageTest {

    @SeleniumWebDriver
    private WebDriver webDriver;

    @Test
    public void testGoogleSearch() {
        webDriver.navigate().to("http://www.google.com");
        assertEquals("Google", webDriver.getTitle());
    }
}
```

The Selenium JUnit 4 Class Runner launches the Selenium web driver or connects to the Selenium server before executing any methods annotated with @BeforeClass. It then autowires any members of the test class or method rules that have been annotated with [@SeleniumBrowser](http://selenium-junit4-runner.btmatthews.com/apidocs/com/btmatthews/selenium/junit4/runner/SeleniumBrowser.html) or [@SeleniumWebDriver](http://selenium-junit4-runner.btmatthews.com/apidocs/com/btmatthews/selenium/junit4/runner/SeleniumWebDriver.html).

### Maven Coordinates

Selenium JUnit 4 Runner is available from [Maven Central](http://search.maven.com/) at the following coordinates:

```xml
<dependency>
    <groupId>com.btmatthews.selenium.junit</groupId>
    <artifactId>selenium-junit4-runner</artifactId>
    <version>1.0.0</version>
    <type>test</type>
</dependency>
```