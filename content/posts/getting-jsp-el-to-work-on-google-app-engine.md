---
title: "Getting JSP EL to work on Google App Engine"
slug: "getting-jsp-el-to-work-on-google-app-engine"
author: "Brian Matthews"
date: 2010-01-04T00:00:00+00:00
year: "2010"
month: "2010/01"
tags: ["jsp","app-engine"]
categories: ["programming"]
draft: false
---

I am developing an Spring 3.0 based web application that I intend to deploy to http://code.google.com/appengine/[Google App Engine]. But I’ve been banging my head against a brick wall for a day now trying to figure out why my ${xxx} expressions are not evaluating correctly.

The source of the problem seems to be a bug with http://code.google.com/appengine/[Google App Engine]. It would appear that even though I am using a Servlet 2.5 deployment descriptor that Google App Engine is behaving as if it was Servlet 2.3.

In Servlet 2.3 JSP expression language was not evaluated by default and it had to be explicitly enabled using the following below. However, since Servlet 2.4 the default behaviour is to evaluate the JSP expression language.

```jsp
<%@ page isELIgnored="false" %>
```

So adding the above directive to my JSP page sorted my http://code.google.com/appengine/[Google App Engine] woes. But not before I’d developed some nasty bruising on my forehead.