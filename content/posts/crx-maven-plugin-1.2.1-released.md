---
title: "CRX Maven Plugin 1.2.1 Released"
date: 2016-02-15T00:00:00+00:00
author: "Brian Matthews"
draft: false
slug: "crx-maven-plugin-1.2.1-release"
year: "2016"
month: "2016/02"
tags: ["chrome-extension"]
categories: ["programming"]
---

This release of the [CRX Maven Plugin](http://crx-maven-plugin.btmatthews.com/) addresses issues some people were having with PEM files exported from [Google Chrome](https://www.google.com/chrome/). The solution involved updating the [Bouncy Castle](http://bouncycastle.org/) dependency to version 1.54.