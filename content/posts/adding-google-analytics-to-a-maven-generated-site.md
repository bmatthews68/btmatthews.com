---
title: "Adding Google Analytics to a Maven generated site"
slug: "adding-google-analytics-to-a-maven-generated-site"
author: "Brian Matthews"
date: 2011-09-17T00:00:00+00:00
year: "2011"
month: "2011/09"
tags: ["maven", "google-analytics"]
categories: ["programming"]
draft: false
---

You can add [Google Analytics](http://www.google.com/analytics) to a site generated with the [Maven Site Plugin](http://maven.apache.org/plugins/maven-site-plugin/) as by adding a *<googleAnalyticsAccountId/>* element containing your Web Property ID.

*site.xml*

```xml
<?xml version="1.0" encoding="ISO-8859-1" ?>
<project
    xmlns="http://maven.apache.org/DECORATION/1.1.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="
        http://maven.apache.org/DECORATION/1.1.0
        http://maven.apache.org/xsd/decoration-1.1.0.xsd"
    name="...">
 
    <googleAnalyticsAccountId>...</googleAnalyticsAccountId>
    
    <!-- ... -->

</project>
```
