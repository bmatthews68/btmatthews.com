---
title: "Setting active profiles when launching Spring Boot application via gradle"
date: 2016-02-21T00:00:00+00:00
author: "Brian Matthews"
year: "2016"
month: "2016/02"
tags: ["gradle", "spring-boot"]
categories: ["programming"]
---
I wanted to use a profile specific application.properties file for my [Spring Boot](http://projects.spring.io/spring-boot/application). But [Gradle](http://gradle.org/) was not picking it up when I ran my launched with the following command:

```bash
gradle bootRun -Dspring.profiles.active=dev
```

The solution turned out be be simply adding the snippet below to my *build.gradle* file. This snippet is telling Gradle to pull in the system properties.

```groovy
bootRun {
    systemProperties = System.properties
}
```