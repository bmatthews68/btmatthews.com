---
title: "How to install Java 6 JDK on Ubuntu"
slug: "how-to-install-java-6-jdk-on-ubuntu"
author: "Brian Matthews"
date: 2009-09-07T00:00:00+00:00
year: "2009"
month: "2009/09"
tags: ["ubuntu"]
categories: ["programming"]
draft: false
---

It couldn’t be easier. Just execute the following commands in your favourite shell:

```bash
sudo apt-get install sun-java6-jdk sun-java6-plugin
update-java-alternatives -s java-6-sun
```

And add the following */usr/lib/jvm/java-6-sun* to the top of the list in your */etc/jvm* file.