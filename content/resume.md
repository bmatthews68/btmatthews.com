---
title: "Brian Matthews - CV/Résumé"
---

### Profile

I am a self-motivated IT professional with over 30 years hands-on experience developing and delivering solutions for the banking/finance, government, insurance, software, telecommunications and transportation industries on Java and .NET stacks.

I have over 20 years project management experience using traditional waterfall-based, iterative and agile software development life cycles. I am a practitioner of the SCRUM agile project management technique.

I have experience coordinating development teams based across multiple geographic locations and time zones. I have worked with off shore development teams in Tunisia, India, Singapore and Philippines and I have worked overseas in UK, USA, France, Japan and Tunisia.

---

### Work History

| Company                                               | From   | To     | Position (Duration)                                    |
| ----------------------------------------------------- | ------ | ------ | ------------------------------------------------------ |
| [Synchronoss](https://synchronoss.com/)               | Jan 16 | —      | Contract Software Engineer (4+ years)                  |
| [eir](https://www.eir.ie/)                            | Jan 14 | Dec 15 | Contract Portal Team Lead (2 years)                    |
| [Daon](https://www.daon.com/)                         | Sep 13 | Dec 13 | Contract Software Engineer (3 months)                  |
| [Realex Payments](https://www.globalpaymentsinc.com/) | Feb 13 | Aug 13 | Contract Software Engineer (6 months)                  |
| [Fujitsu](http://www.fujitsu.com/)                    | Jan 13 | Feb 13 | Contract Software Engineer (1 month)                   |
| [Newbay](https://synchronoss.com/)                    | Jan 12 | Oct 12 | Contract Software Engineer (9 months)                  |
| [LeasePlan](https://www.leaseplan.com/en-ie/)         | Jul 11 | Jan 12 | Contract Software Engineer (6 months)                  |
| [D&B](https://www.dnb.com/ie/)                        | May 11 | Jul 11 | Contract Technical Architect (3 months)                |
| [Fujitsu](http://www.fujitsu.com/)                    | Jun 10 | Apr 11 | Contract SOA Architect (10 months)                     |
| [Corvil](https://www.corvil.com/)                     | Oct 09 | Jun 10 | Contract Software Engineer (8 months)                  |
| [Vodafone](https://www.vodafone.com/)                 | Feb 09 | Sep 09 | Contract Technical Architect (7 months)                |
| [TerraNua](https://www.terranua.com/)                 | Aug 06 | Jul 08 | Director / Architect (2 years)                         |
| [Fidelity Investments](https://www.fidelity.com/)     | Oct 05 | Jul 06 | Information Security Consultant / Architect (9 months) |
| [KVH](https://www.colt.net/)                          | Oct 03 | Sep 05 | Lead System Architect (2 years)                        |
| [Fidelity Investments](https://www.fidelity.com/)     | Jun 00 | Sep 03 | Principal Consultant / Architect (3 years)             |
| International Financial Systems                       | Aug 99 | May 00 | Software Development Manager (9 months)                |
| [AT&T Labs](https://www.att.com/)                     | Oct 98 | Jul 99 | Contract Technical Lead (9 months)                     |
| [IBM](https://www.ibm.com/)                           | Oct 97 | Sep 98 | Contract Project Lead (11 months)                      |
| [Microsoft](https://www.microsoft.com/)               | Apr 96 | Jul 97 | Contract Software Engineer (1 year)                    |
| [Lotus](https://www.ibm.com/)                         | Sep 95 | Mar 96 | Contract Software Engineer (6 months)                  |
| [Lehman Brothers](http://www.lehman.com/)             | Aug 94 | Aug 95 | Contract Project Lead (1 year)                         |
| Hewlett-Packard                                       | Apr 94 | Jul 94 | Contract Software Engineer (3 months)                  |
| [IBM](https://www.ibm.com/)                           | Oct 93 | Mar 94 | Contract Technical Lead (5 months)                     |
| British Rail                                          | Mar 93 | Sep 93 | Contract Software Engineer (6 months)                  |
| [AT&T Istel](https://www.att.com/)                    | Sep 92 | Jan 93 | Contract Software Engineer (4 months)                  |
| Polydata                                              | Apr 89 | Aug 92 | Senior Software Engineer (3 years)                     |

---

### Current Areas of Interest

I am currently researching:

* Functional programming with Scala and Akka

* MongoDB NoSQL databases

* SBT and Gradle build automation tools

---

### Open Source Projects

I am the sole contributor to the following Open Source projects:

* [Selenium JUnit 4 Runner](https://github.com/bmatthews68/selenium-junit4-runner) - a JUnit class runner that reduces some of the boiler-plate required to create integration tests with Selenium.
* [CRX Maven Plugin](https://github.com/bmatthews68/crx-maven-plugin) - package and sign Google Chrome extensions.
* [LDAP Maven Plugin](https://github.com/bmatthews68/ldap-maven-plugin) - import/export data to/from a LDAP directory and run an embedded LDAP directory server within the build life cycle.
* [e-Mail Server Maven Plugin](https://github.com/bmatthews68/emailserver-maven-plugin) - run an embedded e-mail server within the build life cycle supporting Greenmail, Dumbster and SubEthaSMTP.
* [In-Memory Database Maven Plugin](https://github.com/bmatthews68/inmemdb-maven-plugin) - run an embedded database server with in the build life cycle supporting HSQLDB,H2 and Derby.

---

### Detailed Work History

#### Contract Software Engineer @ Synchronoss

Jan 16 - Present | Dublin, Ireland

I have worked on several of the core services, and many of the shared libraries, of Synchronoss'
Personal Cloud product suite (https://synchronoss.com/products/cloud/personal-cloud-solution/) for
mobile telecommunications providers such as Verizon and BT.

* Digital Vault — provides APIs to manage meta-data about content backed up from users' mobile
and desktop devices.

* Smart Albums — uses geo-location and timestamp information in images and videos to
automatically create and maintain albums as content is being uploaded to Digital Vault.

* Cloud Share — provides APIs to share uploaded content with other users.

* Message Minder — provides APIs to back-up, restore and manage call logs and SMS, MMS and
RCS messages from users' mobile devices.

* Authentication, Tokens & Provisioning — provides APIs to provision and authenticate users and to
refresh authentication tokens.

##### Achievements

* Re-designed the processing pipe-line of the Smart Album component to increase through-put and
avoid the need for expensive rollback operations.

* Migrated Message Minder and ATP components to Spring Boot and upgraded JAX-RS
implementations to Jersey 2.

* Implemented support for RCS messages in Message Minder.

* Re-designed the persistence mechanisms in Message Minder to address design flaws in the
original product that was leading to data corruption in Cassandra database clusters.

* Added support for family sharing to Digital Vault which allows family members to share quota and
content.

* Introduced the bill-of-materials concept to ensure consistency in the use of 3rd party libraries
across all core services and shared libraries.

**Programming Languages:** Java, Groovy

**Frameworks:** Spring, Spring Boot, Spring Cloud, Spring Security, JAX-RS (Jersey), Eureka,
Docker

**Target Environments:** Docker, AWS, Cassandra, MySQL, Oracle, Memcached, RabbitMQ, SwiftMQ,
MacOS, Linux

**Development Tools:** Git, Maven, Gradle, JIRA, Bamboo, Confluence, Stash, AsciiDoctor, Docbook,
Cucumber, Sonar

---

#### Portal Lead (Contract) @ eir

Jan 14 - Dec 15 | Dublin, Ireland

I worked on-site with eir (http://www.eir.ie) leading the development, deployment and integration of portal
projects for business customers (https://advantagemanager.eir.ie).

##### Achievements

* Led the deployment and integration of the Loki Portals
(http://www.leonidsystems.com/products/lokiportals) self-care portal for the VoIP services of the
SIP Trunking, Mobile Extension and Hosted Office projects at eir (http://www.eir.ie).

* Integrated the Loki Portals with the OpenAM single sign-on platform using SAML 2.0.

* Coordinated between the project owners, suppliers and the security, network and server
operations teams.

* Identified and ensured the resolution of security and performance issues in the vendor supplied
products.

* Coordinated with the off-shore development team responsible for customizing the look & feel of
Loki Portals to adhere to the eir branding.

* Created high- and low-level design documentation for the overall solution.

* Part of the RFP team that evaluated and selected converged billing analytics and presentment
tools for corporate customers. The selected product was Optimiser from Soft-ex. Afterwards, I was
responsible for integrating Optimiser into the eir Business portals.

* Deployed the single sign-on platform (OpenAM) for eir Business Online portals and integrated it
with the VoIP self-care and bill analytics solutions.

* Created a web application for use by customers and eir staff to manage access to eir Business
Online portal features on behalf of users.

* Created RESTful and SOAP web services to support user provisioning by internal order
processing systems and external vendor platforms.

**Programming Languages:** Java, JavaScript, Ruby, PHP

**Frameworks:** Spring, Spring Security, Spring Security SAML, Spring Web Services, Thymeleaf,
Smarty Templates, jQuery, AngularJS, Bootstrap, SimpleSAMLphp

**Target Environments:** Redhat Linux, Windows Server, Tomcat, SQL Server, MySQL, OpenAM, OpenDJ,
Memcached, Postfix

**Development Tools:** IntelliJ, Git, Maven, Grunt, Jenkins, Chef, Vagrant, Docbook

---

#### Contract Software Engineer @ Daon

Sep 13 - Dec 13 |  Dublin, Ireland

At Daon I worked independently developing features for their IdentityX product which uses biometric and multi-factor authentication to secure transactions on mobile devices.

##### Achievements

* Migrated the bulk of the IdentityX code-base from a legacy Ant-based to a Maven-based build.

* Implemented the support for RSA SecurID based authentication for IdentityX.

* Introduced the Jasmine test framework to unit test the server-side JavaScript scripts glued together many of the components of the IdentityX server.

**Programming Languages:** Java, JavaScript

**Frameworks and Libraries:** Spring, Jasmine

**Target Environment:** Redhat Linux, Windows Server, Tomcat, Oracle, SQL Server, MySQL

**Development Tools:** Eclipse, Subversion, Maven, Ant, Jenkins

---

#### Contract Software Engineer @ Realex Payments

Feb 13 - Aug 13 | Dublin, Ireland

At Realex Payments I was a member of the Agile team that is developing Hosted Payments Page and the Fraud Management component of Real Control. Hosted Payments Page provides a white-labelled check-out facility for online merchants. Real Control is the tool used by merchants to configure their Realex services, access reports and review transaction history.

**Programming Languages:** Java, JavaScript

**Frameworks and Libraries:** Spring, Spring Security, Spring Integration, Thymeleaf, myBatis SQLMaps, Apache Jackrabbit, jQuery, Handlebars

**Target Environment:** SpringSource tcServer, Redhat Enterprise Linux, Microsoft SQL Server

**Development Tools:** SpringSource Tool Suite, JIRA, Confluence, Jenkins, Nexus, Maven

---

#### Contract Software Engineer @ Fujitsu

Jan 13 - Feb 13 | Dublin, Ireland

I was hired by Fujitsu for 4 weeks to complete the document generation and management component of a solution for the Irish Department of Transport that managed licensing for road haulage operators.

**Programming Languages:** Java

**Frameworks and Libraries:** Spring, JSF, OpenCMIS, Apache FOP

**Target Environment:** Alfresco

**Development Tools:** Eclipse, Subversion, Nexus, Maven

---

#### Contract Software Engineer @ Newbay

Jan 12 - Oct 12 | Dublin, Ireland

At Newbay I was a member of an Agile team that developed and maintained SyncDrive. SyncDrive is a white label product offered to mobile phone operators to allow users synchronize content between their PCs, mobile devices and cloud based storage.

##### Achievements

* Resolved high priority defects in order to complete the first version of SyncDrive for Mac OS X and deliver on time to the operator.

* Extensively re-factored the code-base to separate presentation, business logic and data concerns. This was done primarily to eliminate inherent race conditions in the synchronization process. But I had the secondary goal of making it possible to write unit tests.

**Programming Languages:** Objective-C

**Frameworks and Libraries:** CoreData, Cocoa, OSX FUSE, OCMock, Growl

**Target Environment:** Mac OS X 10.6+

---

#### Contract Software Engineer @ LeasePlan

Jul 11 - Jan 12 | Dublin, Ireland

At LeasePlan I was a member of an Agile team that re-engineered LeasePlan's Internet Quotation web application to improve the user experience and address security concerns raised by external auditors.

##### Achievements

* Introduced Selenium integration tests into the automated Maven build

* Introduced JIRA and GreenHopper for bug tracking and task management

* Migrated code base from Spring 2 to Spring 3

* Implemented support for dynamic look and feel using Apache Jackrabbit as the content repository to allow individual business units and brokers have distinct look and feels

* Addressed performance issues when proxying remote content (car images) provided by 3rd party systems by introducing caching and image scaling

**Programming Languages:** Java, JavaScript

**Frameworks and Libraries:** Spring, Spring Security, Struts 2, iBatis SQLMaps, Apache Jackrabbit

**Target Environment:** iSeries, WebSphere, WebSphere MQ

**Development Tools:** Maven, Subversion, JIRA, Greenhopper, Artifactory, Selenium, Eclipse

---

#### Contract Technical Architect @ D&B

May 11 - Jul 11 | Dublin, Ireland

I was taken on by D&B to be an architect on user interface and input handler components of their new Data Supply Chain infrastructure. The Data Supply Chain infrastructure is responsible for processing all inbound data used by D&B to accumulate business intelligence, derive linkage information and calculate credit scores. The project has not progressed past the requirements gathering phase when I left.

---

#### Contract SOA Architect @ Fujitsu

Jun 10 - Apr 11 | Dublin, Ireland

At Fujitsu I designed and implemented solutions for the Irish Department of Transport and the Irish Courts Service using the principles of Service Oriented Architecture.

##### Achievements

* Designed the integration for the Department of Transport with its equivalents in other EU jurisdictions to share driver, vehicle and owner information using Oracle SOA Suite 10g.

* Implemented web services using Oracle SOA Suite 10g to allow the Road Safety Authority and Taxi Regulator access the driver and vehicle database maintained by the Department of Transport.

* Implemented a web service and front end to allow vehicle owners recover the PIN they need to pay motor tax online.

* Proposed the development toolset and open source technology stack for the Irish Courts Service.

* Designed and led the implementation a proof of concept for the Irish Courts Service to allow plaintiffs seek judgements for liquidated sums online using JBoss, Spring, Spring Web Services, Hibernate and JBoss ESB

* Upgraded the integration of Murex trading and SWIFT settlement systems at KBC Bank

**Programming Languages:** Java, Shell Scripting, BPEL, JavaScript

**Frameworks and Libraries:** Spring, Spring Security, Spring Webflow, Spring Web Services, Hibernate, EHChache, jBPM, Drools

**Target Environment:** Solaris, WebSphere MQ, OC4J, JBoss, Oracle SOA Suite, JBossESB, Apache, OpenLDAP, Active Directory, MySQL, Ingres, Oracle

**Development Tools:** Maven, ANT, Fisheye, Bamboo, Crucible, Proximity, Grinder, JMeter, Benerator, Eclipse

---

#### Contract Software Engineer @ Corvil

Oct 09 - Jun 10 | Dublin, Ireland

I developed decoders for Corvil to handle market data feed, trading and middle-ware protocols in order to perform gap detection and message correlation within their latency analysis tools.

##### Achievements

* Implemented a generic template driven decoder that exceeded the performance targets

* Implemented decoders to handle protocols for the Deutsche Börse, London, NASDAQ, NYSE, Tokyo and Osaka exchanges

* Implemented a decoder for Tibco Rendezvous by reverse engineering the sample traffic

**Programming Languages:** C++, PERL, Python

**Frameworks and Libraries:** Boost, STL, Expat, Xerces

**Target Environment:** BSD

**Development Tools:** g++, Subversion, JIRA, Fisheye, Bamboo, Crucible, Valgrind

---

#### Contract Technical Architect @ Vodafone

Feb 09 - Sep 09 | London, UK

I was the technical architect for My Web, Vodafone's new mobile portal that evolved into Vodafone 360. It was originally launched for Egypt, Germany, Greece, Ireland, Italy, Netherlands, Portugal, Spain, South Africa, Turkey and UK in 2009.

##### Achievements

* Re-designed the software architecture to ensure the system would meet non-functional performance and stability requirements to support an initial active user base of 7.5m with a peak load of 1,600 page views per second

* Migrated the build and improved the automation from ANT to Maven 2

**Programming Languages:** Java, PHP, JavaScript

**Frameworks and Libraries:** Spring, Spring LDAP, Struts, Hibernate, EHCache, JGroups, Apache Commons, OSGi, Ext/JS

**Target Environment:** JBoss AS, Apache Felix, Apache HTTPD Server, Oracle 10g, Solaris

**Development Tools:** Maven, Hudson,Archiva, Eclipse, Subversion, Grinder, JProbe, Mercury Quality Centre

---

#### Director / Architect @ TerraNua

Aug 06 - Jul 08 | Dublin, Ireland

At TerraNua I was mainly responsible for designing the architecture and overseeing the implementation of MyComplianceOffice. MyComplianceOffice is a Software as a Service (SaaS) hosted/multi-tenant solution that allows US-based registered investment advisors and hedge funds manage their compliance related business processes. MyComplianceOffice was built using portal server, workflow and document management technologies.

##### Achievements

* Designed the physical and software architecture for MyComplianceOffice

* Recruited and led the development team for release 1.0

* Led the architecture team

* Established the engineering practices

* Introduced SCRUM to manage the development phase of the project

* Recruited and mentored an offshore development team in Tunisia

* Introduced a Wiki to manage developer documentation

* Introduced continuous integration (using Continuum)

**Programming Languages:** Java, JavaScript

**Frameworks and Libraries:** Spring, Acegi, Spring Web Services, Apache Axis, Spring LDAP, Hibernate, Compass, Lucene, Quartz, Drools, JUG, CGLIB, EhCache, Shark

**Target Environment:** Jetspeed 2, IBM WebSphere, Netscape iPlanet, SunONE Directory Server, Documentum, Oracle 10g, Solaris
**Development Tools:** Maven, Continuum, Archiva, Eclipse, Clearcase, Apache HTTP Server, Apache Tomcat, Oracle XE, Windows, Sharepoint, JIRA, LoadRunner, QuickTest Pro, MediaWiki

---

#### Information Security Consultant / Architect @ Fidelity Investments

Oct 05 - Jul 06 | Dublin, Ireland

As an Information Security Consultant at Fidelity Investments I was the architect supporting the teams responsible for developing and maintaining Fidelity Investments' enterprise-wide automated access provisioning, risk management and reporting system. The core components were an intranet facing application for raising and processing access requests and workflow engine that integrated the various 3rd party solutions and automate the provisioning processes. The intranet facing application was developed using ASP.NET and the workflow engine was implemented using C# and NxBRE.

##### Achievements

* Introduced Test Driven Development (TDD) to the automated provisioning team

* Designed and implemented a new automation engine to provision user access requests

**Programming Languages:** C#

**Frameworks and Libraries:** .NET, ASP.NET, NxBRE

**Target Environment:** IIS, Active Directory, Oracle 9i, Sun Identity Manager, BMC Enterprise Security Station, Windows 2003 Server, Solaris

**Development Tools:** Visual Studio, Clearcase, ClearQuest

---

#### Lead System Architect @ KVH

Oct 03 - Sep 05 | Tokyo, Japan

I was seconded to a private telecommunications company owned by Fidelity Investments called KVH. At KVH I reported to the CIO but also worked closely with the CTO and CFO architecting the integration of and supporting the implementation of Business and Operations Support Systems. The majority of the applications at KVH were deployed on Windows based platforms.

##### Achievements

* Created and maintained the blueprint and roadmap for the overall architecture of the OSS/BSS platform

* Conducted product evaluations and engaged in vendor negotiations

* Designed and supported the development of eKVH - a Business to Consumer (B2C) portal developed using BEA WebLogic Portal by an outsourced team in India

* Designed and developed a Business to Employee (B2E) portal implemented in Struts

**Programming Languages:** Java

**Frameworks and Libraries:** Struts, Apache FOP, Hibernate, Velocity

**Target Environment:** BEA WebLogic Portal, Tomcat, webMethods, Siebel, Oracle eBusiness Suite, Portal Infranet, Mucromuse Netcool, Infovista

**Development Tools:** Eclipse, CVS, LoadRunner, QuickTest Pro

---

#### Principal Consultant / Architect @ Fidelity Investments

Jun 00 - Sep 03 |  Dublin, Ireland

At Fidelity Investments I played a leading role in three major product developments:\n http://www.planviewer.co.uk/[PlanViewer] is provided by Fidelity International Limited (FIL) to allow members, sponsors and administrators of defined contributions pension schemes to view balances, review transaction history, switch out of existing investments or change their future contribution mix. PlanViewer was a J2EE application developed using Struts. http://personal.fidelity.com/accounts/activetrader[ActiveTrader] Pro is a desktop trading application provided by Fidelity eBusiness for the high net worth and active trader market segments to access their brokerage accounts, place trades, receive streaming quotes and review market news. ActiveTrader Pro was a Windows desktop application developed in C++ using ActiveX components. * Fidelity Online Xpress+ (FOX+) was Fidelity Investments' original desktop trading application available to all customer segments. FOX+ allowed customers to access their accounts, place trades, receive static quotes and review market news. FOX+ was a Windows desktop application developed in C++.

##### Achievements

* I was a member of the Development Audit Team (DAT) which audited projects to ensure they adhered to best practices from a project management perspective during the project initiation, requirements gathering and solution design phases

* I was a founding member of the Technical Review Board (TRB) which reviewed the proposed architectures and detailed designs of projects to ensure that those projects were technically feasible and following best practices

* Ported PlanViewer from a proprietary model-view-controller framework to Struts 1.1

* Led the project team in Dublin that implemented many of the key components of ActiveTrader Pro

* Designed and implemented the framework for the user interface of ActiveTrader Pro

* Achieved 4.5 out of 5 customer satisfaction ratings from the Active Trader Pro project stakeholders

* Streamlined the configuration management and release engineering practices for FOX+

* Dramatically reduced the size of the downloadable product installer for FOX+ from 12MB to 3MB

* Successfully delivered quarterly releases of FOX+

* Achieved 5 out of 5 customer satisfaction ratings from the FOX+ project stakeholders

**Programming Languages:** Java, C++, JavaScript

**Frameworks and Libraries:** Struts, STL, MFC, RogueWave Libraries, COM/ATL, ADO

**Target Environment:** IBM WebSphere, Sybase, Solaris, Windows 95/NT/ME/2000

**Development Tools:** Eclipse, Visual C++, Clearcase, LoadRunner, WinRunner, ClearQuest, Test Director

---

#### Software Development Manager @ International Financial Systems

Aug 99 - May 00 | Dublin, Ireland

I joined IFS as the Senior Software Architect to design the architecture for a new margin trading system that would replace the company's existing thick client product offering called MarginMan. MarginMan was developed as a Windows desktop application using C++.

##### Achievements

* Designed the architecture for a CORBA based n-tier collateralized margin trading system

* Took on the role of Software Development Manager with responsibility for teams based in Dublin, Singapore and Manila

**Programming Languages:** C++

**Frameworks and Libraries:** Orbix, MFC

**Target Environment:** Windows NT

**Development Tools:** Visual C++, Visual SourceSafe

---

#### Contract Technical Lead @ AT&T Labs

Oct 98 - Jul 99 | Redditch, UK

At AT&T Labs I worked for the IP Technology Organization, which was developing a platform to construct and manage network services called Common Open IP Platform (COIPP). I provided CORBA expertise to the team responsible for implementing the middle-tier components of the provisioning, billing and management systems.

##### Achievements

* Ported the existing components from Orbix to VisiBroker

* Assisted the team that ported the existing components from Windows to Solaris

* Migrated the team's version control solution from PVCS to Clearcase

**Programming Languages:** Java, C++

**Frameworks and Libraries:** Orbix, VisiBroker

**Target Environment:** MQSeries, Oracle, Solaris

**Development Tools:** Visual C++, Sun C++, Clearcase, PVCS

---

#### IBM, Dublin, Ireland

Contractor - Project Lead, Oct 97 - Sep 98

At IBM I worked with the Insurance Solutions Development Centre developing a customer relationship management application for insurance companies called Client Information & Integration System (CIIS).

##### Achievements

* Led the team that designed the architecture for CIIS

* Led the team developed the middle-tier components

**Programming Languages:** Java, C++

**Frameworks and Libraries:** Swing, Orbix, OrbixWeb

**Target Environment:** DB2, Solaris

**Development Tools:** Visual C++, Visual SourceSafe

---

#### Contract Software Engineer @ Microsoft

Apr 96 - Jul 97 | Seattle, WA, USA

At Microsoft I worked for the Infrastructure and Automation Tools team. I was responsible for developing agents to be installed on file, database, e-mail, web and proxy servers to collect usage metrics and in order to predict future server and disk space demands. These agents were installed on over 3,000 servers worldwide.

**Programming Languages:** C++

**Frameworks and Libraries:** MFC

**Target Environment:** SQL Server, Windows NT

**Development Tools:** Visual C++, Visual SourceSafe

---

#### Contract Software Engineer @ Lotus

Sep 95 - Mar 96 | Dublin, Ireland

At Lotus I worked for the Global QA team that developed test, automation and localization tools used to test and localize Lotus' office application suite.

##### Achievements

* Developed plug-ins to instrument Lotus' custom controls

* Unified the code-base to eliminate the need for separate builds for each flavour of Windows

**Programming Languages:** C++

**Target Environment:** Windows 3.x, Windows 95, Windows NT

**Development Tools:** Visual C++, PVCS, Lotus Notes

---

#### Contract Project Lead @ Lehman Brothers

Aug 94 - Aug 95 | London, UK

At Lehman Brothers we developed and maintained applications to support the purchasing, goods inwards and accounting departments of Lehman Brothers in London.

##### Achievements

* Recruited and led the development team

* Ported existing applications from OS/2 to Windows 3.x

* Carried out maintenance and implemented enhancements to meet changing business practices

**Programming Languages:** C++

**Frameworks and Libraries:** Object Windows Library

**Target Environment:** Lotus Notes, Sybase, Windows 3.x, OS/2

**Development Tools:** Borland C++, CSet++, PVCS

---

#### Contract Software Engineer @ Hewlett-Packard

Apr 94 - Jul 94 | Grenoble, France

At Hewlett-Packard I was part of a small team that ported a product called Omnishare from specialized hardware to run on a standard IBM compatible PC. Omnishare was a conferencing tool that allowed users to share and annotate documents using the same telephone line for voice and data.

**Programming Languages:** C++

**Frameworks and Libraries:** MFC

**Target Environment:** Windows 3.x

**Development Tools:** Visual C++, Visual SourceSafe

---

#### Contract Software Engineer @ IBM

Oct 93 - Mar 94 | Dublin, Ireland

At IBM I provided consultancy to the team developing a data warehousing tool called DataRefresher. When I joined the DataRefresher was failing to meet the acceptance criteria set by the QA team due to significant memory leaks, race conditions and inter-process communication issues.

##### Achievements

* I identified the sources of all major defects that were preventing the QA team accept the build.

* Re-introduced release engineering processes that had been abandoned by the development team.

**Programming Languages:** C++

**Target Environment:** OS/2, DB2, Communications Manager

**Development Tools:** CSet++

---

#### Contract Software Engineer @ British Rail

Mar 93 - Sep 93 | Darlington, UK

At British Rail I was part of the team developing a client server application called Advanced Transmanche Operations Management System (ATOMS). ATOMS was the passenger booking and rolling stock management system developed for British Rail, SNCF France and SNCF Belgium to operate services running through the Euro Tunnel.

##### Achievements

* Designed and implemented the framework for the ATOMS user interface.

**Programming Languages:** C++

**Frameworks and Libraries:** MFC

**Target Environment:** Windows 3.x, Oracle

**Development Tools:** Visual C++, PVCS

---

#### Contract Software Engineer @ AT&T Istel

Sep 92 - Jan 93 | Redditch, UK

At AT&T Istel I implemented a light-weight object request broker for Windows that allowed inter-process communication on a personal computer and with a server process running on an Unix platform. The object request broker pre-dated Common Object Request Broker Architecture (CORBA) and was based on Advanced Network Systems Architecture (ANSA).

##### Achievements

* Developed the inter-process communication for co-located Windows applications using Dynamic Data Exchange (DDE).

* Developed the client-side communication between the Windows applications and server processes over a serial connection.

**Programming Languages:** C++

**Target Environment:** Windows 3.x, Unix

**Development Tools:** Visual C++, PVCS

---

#### Senior Software Engineer @ Polydata

Apr 89 - Aug 92 | Dublin, Ireland

At Polydata I designed and developed bespoke applications for petro-chemical companies including DOW Chemical , DuPont , ICI , Bayer and Elf Atochem. These applications were electronic catalogues describing the material properties of the plastics manufactured by those companies.

##### Achievements

* Consolidated and re-factored the existing source code developed for different customers into a single code base.

* Automated the release engineering process.

**Programming Languages:** Pascal, C, C++, Assembler

**Target Environment:** MS-DOS

**Development Tools:** Turbo Pascal, Turbo C++, RCS

---

### Qualifications & Training

**B.Sc. in Computer Applications**

* Dublin City University

* Graduated with honours in November 1990

---

### References

Available upon request