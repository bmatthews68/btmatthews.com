---
title: "About Brian Matthews"
---
I am a freelance Technical Architect and Developer with over 30 years experience designing and developing solutions on the Java, .NET, Windows and Unix stacks mainly in Java and C/C++ using both proprietary and open source frameworks and components.

Since graduating with a [B.Sc. in Computer Applications](http://www.dcu.ie/prospective/deginfo.php?classname=CA) from [Dublin City University](http://www.dcu.ie/) in 1990 I have worked mainly within the financial services and telecommunications industries in both permanent and contract roles for the following companies:

* [Synchronoss](https://synchronoss.com/)
* [eir](https://www.eir.ie/)
* [Daon](https://www.daon.com/)
* [Realex Payments](https://www.daon.com/)
* [Fujitsu](http://www.fujitsu.com/ie/)
* Newbay Software - now [Synchronoss](https://synchronoss.com/)
* [LeasePlan](https://www.leaseplan.com/en-ie/)
* [D&B](https://www.dnb.com/ie/)
* Corvil - now [Pico](https://www.pico.net/)
* [Vodafone](https://www.vodafone.com/)
* TerraNua - now [MyCompianceOffice](https://mco.mycomplianceoffice.com/)
* Fidelity Investments
* KVH - now [COLT](https://www.colt.net/)
* International Financial Systems
* AT&T
* [IBM](https://www.ibm.com/)
* Microsoft
* Lotus
* [Lehman Brothers](http://www.lehman.com/)
* Hewlett-Packard
* British Rail
* Polydata