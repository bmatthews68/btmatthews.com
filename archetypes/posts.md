---
title: "{{ replace .Name "-" " " | title }}" 
slug: "{{ .Name }}"
author: "Brian Matthews"
date: {{ .Date }} 
year: "{{ dateFormat "2006" .Date }}" 
month: "{{ dateFormat "2006/01" .Date }}"
tags: []
categories: []
draft: false 
---

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam accumsan lectus ac dolor vulputate, id eleifend dolor dictum. Nullam vulputate congue purus, ac luctus odio ultrices quis. Proin porta tempor scelerisque. Sed ac erat non leo feugiat mattis. Aliquam mattis erat vel sodales tempor. Curabitur sollicitudin ante a quam faucibus varius. Vestibulum quis purus interdum lorem cursus interdum aliquam tempus sem. Ut vitae lorem eu libero porta scelerisque

<!--more-->

Fusce euismod luctus risus quis auctor. Praesent interdum, odio id lacinia malesuada, ipsum orci tempus ligula, non rhoncus lorem mauris in dolor. Cras sed lectus eros. Ut velit quam, mattis eu euismod sodales, aliquet id justo. Proin non facilisis est. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vestibulum sed auctor massa, dapibus imperdiet tortor. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Morbi elementum facilisis est. Phasellus varius nisi vel diam dignissim interdum. Morbi tincidunt venenatis commodo. Proin malesuada pulvinar erat, vitae placerat diam pellentesque quis. Proin vel porttitor nibh. Aenean vitae faucibus orci, et hendrerit ligula. Aenean ac ex risus.

Proin in neque at dolor accumsan tempor in eget nibh. Vivamus tempus urna id diam vestibulum pharetra. Nunc rhoncus elit sollicitudin purus placerat vulputate. Etiam tempus at metus eu pulvinar. Vestibulum tempus libero orci. Integer auctor sem nec euismod sodales. Duis id orci a libero tempus viverra sed in arcu. Mauris tempus sollicitudin sapien, ut tristique massa porttitor sit amet.

Ut convallis sem sit amet nulla tristique, id aliquet quam placerat. Nullam tristique, erat sed pulvinar blandit, sem ex vehicula massa, in condimentum mauris odio ac ex. Aliquam suscipit, dolor non posuere sodales, dui urna malesuada nisi, eget tristique eros orci at nulla. Nunc pulvinar interdum lacus, nec faucibus nisi mattis dignissim. Nulla et sollicitudin elit. In tincidunt, mi id commodo consectetur, nisl ante vestibulum quam, nec sodales sapien neque ullamcorper ante. Morbi pharetra lacinia nunc ac hendrerit. Mauris hendrerit mauris a odio eleifend sagittis. Quisque in venenatis urna, a ornare risus. Proin id odio nec sapien porta pretium in non massa.

Nunc ut augue sed est lacinia euismod. Donec pulvinar neque nulla, in fringilla est varius eget. Cras elementum ipsum in arcu egestas, eget suscipit eros commodo. Etiam eros mauris, euismod sit amet malesuada eu, fringilla at elit. Sed tempus, magna ut fermentum efficitur, lorem neque scelerisque dui, vel fringilla sapien libero ac nisi. Nullam mollis ultrices nisl, sit amet ornare orci accumsan at. Morbi ultrices sed justo et eleifend. Donec eget dolor quis dui placerat condimentum sit amet quis ante. In hac habitasse platea dictumst. Nullam rhoncus eget purus et imperdiet. Suspendisse viverra auctor felis sed vehicula. Sed a efficitur nibh.